

#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;

// Simplex 2D noise

vec3 permute(vec3 x){return mod(((x*34.)+1.)*x,289.);}

float snoise(vec2 v){
    const vec4 C=vec4(.211324865405187,.366025403784439,
    -.577350269189626,.024390243902439);
    vec2 i=floor(v+dot(v,C.yy));
    vec2 x0=v-i+dot(i,C.xx);
    vec2 i1;
    i1=(x0.x>x0.y)?vec2(1.,0.):vec2(0.,1.);
    vec4 x12=x0.xyxy+C.xxzz;
    x12.xy-=i1;
    i=mod(i,289.);
    vec3 p=permute(permute(i.y+vec3(0.,i1.y,1.))
    +i.x+vec3(0.,i1.x,1.));
    vec3 m=max(.5-vec3(dot(x0,x0),dot(x12.xy,x12.xy),
    dot(x12.zw,x12.zw)),0.);
    m=m*m;
    m=m*m;
    vec3 x=2.*fract(p*C.www)-1.;
    vec3 h=abs(x)-.5;
    vec3 ox=floor(x+.5);
    vec3 a0=x-ox;
    m*=1.79284291400159-.85373472095314*(a0*a0+h*h);
    vec3 g;
    g.x=a0.x*x0.x+h.x*x0.y;
    g.yz=a0.yz*x12.xz+h.yz*x12.yw;
    return 130.*dot(m,g);
}


// draw a circle
#define drawCircle(st,pos,size)smoothstep(0.,400./u_resolution.y,size-length(pos-st))

void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution.xy;
    st.x*=u_resolution.x/u_resolution.y;
    st.x+=snoise(st+vec2(u_time*0.9,0.0));

    vec3 canvas=vec3(0.);

    float cs=(snoise(st*6.+vec2(0,u_time*1.33))*.5+.75);

    float circ=drawCircle(st,vec2(.25),cs);
    float circ2=drawCircle(st,vec2(.75),cs);

    vec3 circColor=vec3(0.0, 1.0, 0.5);
    vec3 circColor2=vec3(0.7, 0.3, 0.9);

    canvas=mix(canvas,circColor2,circ2);

    canvas=mix(canvas,circColor,circ);

    gl_FragColor=vec4(canvas,1.);
}
