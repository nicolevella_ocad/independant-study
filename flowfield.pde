// This code is based on Daniel Shiffman's FlowField tutorial:
// Coding Challenge #24: Perlin Noise Flow Field
// https://youtu.be/BjoM9oKOAKY
//

// create a flowfield class and give it some parameters
public class FlowField {
  PVector[] vectors;
  int cols, rows;
  float inc = 0.1;
  float zoff = 0;
  int scl;
  int border;

  // initalize the flowfield, using a resolution as an argument
  FlowField(int res) {
    scl = res;
    cols = floor(width / res)+1;
    rows = floor(height / res)+1;
    // border = floor(cols/4);
    vectors = new PVector[cols * rows];
    //    println(cols+", "+rows);
  }

  // function to update the flowfield and the vectors within
  void update(int seed, float f, float m) {
    float xoff = 0;
    for (int y = 0; y < rows; y++) { 
      float yoff = 0;
      for (int x = 0; x < cols; x++) {
        noiseSeed(seed);

        // this is the main formula that generates my spiral/ring patterns
        float angle = noise(xoff, yoff, zoff) * f;

        PVector v = PVector.fromAngle(angle);
        v.setMag(m);
        int index = x + y * cols;
        vectors[index] = v;
        xoff += inc;
      }
      yoff += inc;
    }
    zoff += 0.0004;
  }
  void display() {
    stroke(0);
    noFill();

    for (int y = 0; y < rows; y++) { 
      for (int x = 0; x < cols; x++) {
        int index = x + y * cols;
        PVector v = vectors[index];
        pushMatrix();
        translate(x * scl, y * scl);
        vertex(0, 0);
        rotate(v.heading());
        vertex(scl, 0);
        line(0, 0, scl, 0);
        popMatrix();
      }
    }
  }
}
