

import processing.pdf.*;
import com.hamoid.*;

VideoExport videoExport;

int scaleFactor = 4; // upscale the pdf export

int SEED = 333; // noise seed

float FORCE = 1500; // force of vectors
float MAG = 3; // magnitude of vectors

int len = 1; // length of particle
int res = 99; // resolution of flow field, more points per line = more curves
int amt = 222; // amount of particles to drop on each loop, affects amount of lines
int loops = 444; // amount of updates to flowfield, affects length of lines

int layer;

void setup() {
  size(2000, 2000, P2D);
  layer = 0;
  //videoExport = new VideoExport(this);
  //videoExport.startMovie();
  
  render();
}

void draw() {

}

void render() {
  resetMatrix();
  background(255);

  // generate the flowfield
  FlowField flowfield;
  flowfield = new FlowField(res);

  // create some particles, in an array particles
  ArrayList<Particle> particles;
  particles = new ArrayList<Particle>();

  for (int i = 0; i < amt; i++) {
    PVector start = new PVector(random(width), random(height));
    particles.add(new Particle(start, len));
  }
  for (int i=0; i<loops; i++) {
    flowfield.update(SEED, FORCE, MAG);
    for (Particle p : particles) {
      p.follow(flowfield);
      p.run();
    }
    // videoExport.saveFrame();
  }


}

void keyPressed() {
  if (key==49) { // 1
    SEED = floor(random(1000));
    render();
  }

  if (key==50) { // 2
    render();
  }

  if (key==51) { // 3
    savePDF();
  }
}

void savePDF() {
  layer++;
  PGraphics hires = createGraphics(
    width, 
    height, 
    PDF, 
    "_PDF"+layer+".pdf");
  println("Saving PDF file...");
  beginRecord(hires);
  background(255);
  render();
  endRecord();
  println("Finished");
}
