
import processing.pdf.*;

int skip = 2;
int border = 666;
int depthFactor = 8;
int scaleFactor = 4;

String imgname = "SPIRAL-SIMPLE"; // name of image file to use as source
String filetype = "jpg"; 

int seed, scaleWidth, scaleHeight;
PImage img;

void settings() {
  smooth();
  img = loadImage(imgname+"."+filetype);

// scale canvas to image size
  scaleWidth = img.width+border;
  scaleHeight = img.height+border;

// let's always make sure the dimensions are divisible by 2
  if (scaleWidth % 2 == 0) {
  } else {
    scaleWidth++;
  }

  if (scaleHeight % 2 == 0) {
  } else {
    scaleHeight++;
  }

  size(scaleWidth, scaleHeight, P3D);
}

void setup() {
  background(255);
  render();
  sphereDetail(2);
}

void draw() {
    // everything is done in the render() function so we can run the algo, and then scale it to high res for export
}

void render() {
  background(255);
  img.loadPixels();

  for (int x = 0; x < img.width; x += skip) {
    for (int y = 0; y < img.height; y += skip) {

      pushMatrix();

      color c = img.get(x, y);
      PVector pos = new PVector(x+(border/2), y+(border/2));
      float b = brightness(c);
      if (b < 235) { 
        pos.z = b;
        strokeWeight(1);
        float newDepth = map(b, 0, 235, -scaleWidth/depthFactor, scaleWidth/depthFactor);
        translate(0, 0, newDepth);
        strokeWeight(1);
        circle(pos.x, pos.y, map(b, 0, 235, 44, 4));
      }

      popMatrix();
    }
  }
}

void keyPressed() {
  switch(key) {
  case 'p':
    savePDF(scaleFactor);
    break;
  case 'l':
    saveLowRes();
    break;
  case 'h':
    saveHighRes(scaleFactor);
    break;
  case '?':
    println("Keyboard shortcuts:");
    println("  l: Save low-resolution PNG");
    println("  h: Save high-resolution PNG");
    println("  p: Save high-resolution PDF");
  }
}

void saveLowRes() {
  println("Saving low-resolution image...");
  save(imgname+"_LOWRES.png");
  println("Finished");
}

void saveHighRes(int scl) {
  PGraphics hires = createGraphics(
    width * scl, 
    height * scl, 
    JAVA2D);
  println("Saving high-resolution image...");
  beginRaw(hires);
  hires.scale(scaleFactor);
  background(255);
  render();
  endRaw();
  hires.save(imgname+"_HIGHRES.png");
  println("Finished");
}

void savePDF(int scl) {
  PGraphics hires = createGraphics(
    width * scl, 
    height * scl, 
    PDF, 
    imgname+"_PDF.pdf");
  println("Saving PDF image...");
  beginRaw(hires);
  hires.scale(scaleFactor);
  background(255);
  render();
  endRaw();
  println("Finished");
}
