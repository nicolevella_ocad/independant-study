// This code is based on Daniel Shiffman's FlowField tutorial:
// Coding Challenge #24: Perlin Noise Flow Field
// https://youtu.be/BjoM9oKOAKY
//


public class Particle {
  PVector pos;
  PVector vel;
  PVector acc;
  PVector previousPos;
  float maxSpeed;

  Particle(PVector start, float maxspeed) {
    maxSpeed = maxspeed;
    pos = start;
    vel = new PVector(0, 0);
    acc = new PVector(0, 0);
    previousPos = pos.copy();
  }

  void run() {
    update();
    edges();
    show();
  }

  void update() {
    pos.add(vel);
    vel.limit(maxSpeed);
    vel.add(acc);
    acc.mult(0);
  }

  void applyForce(PVector force) {
    acc.add(force);
  }

  void show() {
    line(pos.x, pos.y, previousPos.x, previousPos.y);
    updatePreviousPos();
  }


  void edges() {
   // add a border to the edges
    
    if (pos.x > width-200) {
      pos.x = 200;
      updatePreviousPos();
    }

    if (pos.x < 200) {
      pos.x = width-200;    
      updatePreviousPos();
    }

    if (pos.y > height-200) {
      pos.y = 200;
      updatePreviousPos();
    }

    if (pos.y < 200) {
      pos.y = height-200;
      updatePreviousPos();
    }
  }

  void updatePreviousPos() {
    this.previousPos.x = pos.x;
    this.previousPos.y = pos.y;
  }

  void follow(FlowField flowfield) {
    int x = floor(pos.x / flowfield.scl);
    int y = floor(pos.y / flowfield.scl);
    int index = x + y * flowfield.cols;
    PVector force = flowfield.vectors[index];
    applyForce(force);
  }
}
